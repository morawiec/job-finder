# Job-finder

## Technologies

1. [Python 3.6](https://docs.python.org/3/library/index.html)
2. [Beautifulsoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
3. [MongoDB](https://docs.mongodb.com/manual/tutorial/)
4. [MongoEngine](http://docs.mongoengine.org/tutorial.html)
5. [Docker](https://docs.docker.com/engine/docker-overview/)

## Documentation

### Purpose

The general purpose of the project is to explore the most popular online job boards and aggregate them in Document oriented database MongoDB.
The project also provides an easy method to preview collected job offers.
### Project structure

The project consists of config file config.py which lets application user to provide basic information about
job finding preferences and application configuration. Ones can configure the following parameters:
* position = "Software developer" - preferred job position
* location = "London" - preferred job location
* pages = 4 - number of pages to examine
* base_url = "https://www.indeed.co.uk" - job board web page (for now only indeed.co.uk is supported)
* sort = "date" - job sorting type  (for now only sorting by date is supported)

Remaining parameters refers to a database and should be modified judiciously

Project entrypoint is module __main__ which controls application workflow. Workflow is following:
1. HTML files are downloaded asynchronously according to data provided in `config.py` files
2. HTML files are parsed parallelly in order to extract the link to a detailed job description. List of links are returned
3. Based on obtained information from the previous step. HTML pages which contains job detailed description are downloaded asynchronously
4. Detailed job description HTML files are parsed parallelly in order to extract the following information:
    -  job title 
    -  salary
    -  job description
    -  company name
    -  detailed job location
    -  link to the offer

5. Received job offers are now asynchronously inserted into the database. The offer is skipped if already exists in the database

Module `job` contains ODM( object document mapping) for job offer - class Job. Job can be saved or read by `JobRepository` from the module `databas`e.
Repository for the need of the project provides only two functions: save job in database and read from database by job_id. Functions can be executed asynchronously.

Module `parsers` is responsible for scraping HTML pages and running parser in a parallel manner.
In the module we can distinguish three classes - abstract class `CustomParser` which contains abstract method `parse` and two abstract class 
implementations `IndeedJobListParser`, `IndeedDetailedJobDescriptionParser`. Class `IndeedJobListParser` is pretty simple, it parses indeed
HTML page containing a list of job offers and extracts links to the detailed job description, whereas `IndeedDetailedJobDescriptionParser` scraps
detailed job offer in order to gain job data. Besides parser classes, the module contains a mechanism to run parsing parallelly- method `parse_html_pages`.
Method supports following arguments **html_lists** and member of **ParsingStrategies** enum class.

The mechanism for asynchronous HTTP request execution is defined in module requests. The module is used to fetch HTML documents from job board web site.
 

### How to run application

1. Install docker according to documentation(https://docs.docker.com/install/)
2. Build and run docker images  
```bash
(job-finder) michal@michal-VirtualBox:~/repos/job-finder$ docker-compose up -d --build
Building job-finder
Step 1/6 : FROM python:3.6
 ---> 1daf62e8cab5
Step 2/6 : ENV app_path=/usr/src/app
 ---> Using cache
 ---> 6a596ba57f01
Step 3/6 : WORKDIR ${app_path}
 ---> Using cache
...
Starting job-finder_mongo-express_1 ... done
Recreating job-finder               ... done
Starting mongodb                    ... done
Attaching to job-finder_mongo-express_1, job-finder, mongodb
```
3) Check if containers are up and running
```bash
(job-finder) michal@michal-VirtualBox:~/repos/job-finder$ docker ps
CONTAINER ID        IMAGE                   COMMAND                  CREATED             STATUS              PORTS                                                NAMES
a7c0a4168dad        job-finder_job-finder   "./entrypoint.sh"        2 minutes ago       Up 2 minutes                                                             job-finder
c8e1a8e494b0        job-finder_mongo        "/entrypoint.sh"         2 hours ago         Up 2 minutes        0.0.0.0:27017->27017/tcp, 0.0.0.0:28017->28017/tcp   mongodb
109777fcf1a2        mongo-express           "tini -- /docker-ent…"   17 hours ago        Up 2 minutes        0.0.0.0:8081->8081/tcp                               job-finder_mongo-express_1
```
4) Inspect database with use of web browser - localhost:8081
![Screenshot](database-screenshot.png)
5) In order to run application again, One should execute following command
```bash
(job-finder) michal@michal-VirtualBox:~/repos/job-finder$ docker exec job-finder python -m job_finder
```
