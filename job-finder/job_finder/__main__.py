from job_finder.database import insert_jobs_to_database
from job_finder.requests import fetch_pages
from job_finder.parsers import ParsingStrategies, parse_html_pages
from job_finder.config import AppConf as cfg


def build_base_request_urls():
    base_url = cfg.BASE_URL
    job_position = cfg.POSITION.lower().replace(" ", "+")
    location = cfg.LOCATION.lower().replace(" ", "+")
    sort_type = cfg.SORT.lower()
    return [f'{base_url}/jobs?q={job_position}&l={location}&sort={sort_type}&start={page}'
            for page in range(0, cfg.PAGES*10, 10)]


if __name__ == "__main__":
    urls = build_base_request_urls()
    job_list_pages = fetch_pages(urls)

    raw_links_to_details = parse_html_pages(job_list_pages, ParsingStrategies.SCRAP_JOBS_LIST)
    links_to_details = [link for links in raw_links_to_details for link in links]

    detailed_jobs_pages = fetch_pages(links_to_details)
    jobs = parse_html_pages(detailed_jobs_pages, ParsingStrategies.SCRAP_JOB_DETAILS)

    insert_jobs_to_database(jobs)
