from enum import Enum
from typing import List
from multiprocessing import Pool
import urllib.parse as urlparse

from abc import ABC, abstractmethod
from bs4 import BeautifulSoup, element

from job_finder.config import AppConf as conf


def scrap_jobs_list(page: str) -> List[str]:
    parser = IndeedJobListParser(page)
    return parser.parse()


def scrap_for_details(page: str) -> dict:
    parser = IndeedDetailedJobDescriptionParser(page)
    return parser.parse()


def parse_html_pages(pages: List[str], strategy) -> List:
    pool = Pool()
    return pool.map(strategy, pages)


class ParsingStrategies(Enum):
    SCRAP_JOB_DETAILS = scrap_for_details
    SCRAP_JOBS_LIST = scrap_jobs_list


class CustomParser(ABC):
    def __init__(self, page: str):
        self.soup = BeautifulSoup(page, 'html.parser')

    @abstractmethod
    def parse(self):
        pass


class IndeedJobListParser(CustomParser):
    TAGS = {
        "job_title": {"tag": "div", "class": "title"},
    }

    @staticmethod
    def _extract_urls(html_jobs: List[element.Tag]) -> List[str]:
        links = []
        base_url = conf.BASE_URL.lower()
        for job in html_jobs:
            original_link = job.find("a").get("href")
            query_param = urlparse.parse_qs(urlparse.urlparse(original_link).query).get("jk")
            if query_param:
                links.append(f'{base_url}/viewjob?jk={query_param[0]}')
        return links

    def parse(self) -> List[str]:
        jobs = self.soup.find_all(self.TAGS["job_title"]["tag"],
                                  class_=self.TAGS["job_title"]["class"])
        return self._extract_urls(jobs)


class IndeedDetailedJobDescriptionParser(CustomParser):
    TAGS = {
        "location": [
            {"tag": "div",
             "class": "jobsearch-JobMetadataHeader-itemWithIcon icl-u-textColor--secondary icl-u-xs-mt--xs"},
            {"tag": "div",
             "class": "icl-IconFunctional icl-IconFunctional--location icl-IconFunctional--md"},
            {"tag": "span",
             "class": "jobsearch-JobMetadataHeader-iconLabel"}
        ],
        "salary": [
            {"tag": "div",
             "class": "jobsearch-JobMetadataHeader-itemWithIcon icl-u-textColor--secondary icl-u-xs-mt--xs"},
            {"tag": "div",
             "class": "icl-IconFunctional icl-IconFunctional--salary icl-IconFunctional--md"},
            {"tag": "span",
             "class": "jobsearch-JobMetadataHeader-iconLabel"}
        ],
        "title": {"tag": "h3", "class": "icl-u-xs-mb--xs icl-u-xs-mt--none jobsearch-JobInfoHeader-title"},
        "description": {"tag": "div", "class": "jobsearch-jobDescriptionText"},
        "company_name": {"tag": "div", "class": "icl-u-lg-mr--sm icl-u-xs-mr--xs"},
        "link": {"tag": "meta", "class": "indeed-share-url"},
    }

    @staticmethod
    def _extract_id_from_url(link: str) -> str:
        return urlparse.parse_qs(urlparse.urlparse(link).query).get("jk")[0]

    def _extract_data_from_html(self, data_type: str) -> str:
        data_type_under_unambiguous_tag = frozenset(("title", "company_name", "description"))
        data_type_under_ambiguous_tag = frozenset(("salary", "location"))
        data_type_from_metadata = frozenset(("link", "id"))

        if data_type in data_type_under_unambiguous_tag:
            return self._extract_from_unambiguous_tag(data_type)
        elif data_type in data_type_under_ambiguous_tag:
            return self._extract_from_ambiguous_tag(data_type)
        elif data_type in data_type_from_metadata:
            return self._extract_metadata_content(data_type)

    def _extract_metadata_content(self, data_type: str) -> str:
        data = self.soup.find(self.TAGS[data_type]["tag"], id=self.TAGS[data_type]["class"])
        return data.get("content")

    def _extract_from_unambiguous_tag(self, data_type: str) -> str:
        data = self.soup.find(self.TAGS[data_type]["tag"],
                              class_=self.TAGS[data_type]["class"])
        if data:
            return data.get_text()

    def _extract_from_ambiguous_tag(self, data_type: str) -> str:
        divs = self.soup.findAll(self.TAGS[data_type][0]["tag"],
                                 class_=self.TAGS[data_type][0]["class"])
        for div in divs:
            contains_salary = div.find(self.TAGS[data_type][1]["tag"],
                                       class_=self.TAGS[data_type][1]["class"])
            if contains_salary:
                return div.find(self.TAGS[data_type][2]["tag"],
                                class_=self.TAGS[data_type][2]["class"]).get_text()

    def parse(self) -> dict:
        link = self._extract_data_from_html("link")
        id = self._extract_id_from_url(link)
        result = {"description": self._extract_data_from_html("description"),
                  "title": self._extract_data_from_html("title"),
                  "company_name": self._extract_data_from_html("company_name"),
                  "location": self._extract_data_from_html("location"),
                  "salary": self._extract_data_from_html("salary"),
                  "link": link,
                  "job_id": id}
        return result
