from mongoengine import Document, StringField


class Job(Document):
    job_id = StringField(required=True)
    title = StringField(required=True)
    link = StringField(required=True)
    salary = StringField()
    location = StringField()
    description = StringField()
    company_name = StringField()
