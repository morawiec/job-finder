from typing import List

import asyncio
from aiohttp import ClientSession


def run_async(function):
    def wrapper(*args, **kwargs):
        loop = asyncio.get_event_loop()
        return loop.run_until_complete(function(*args, **kwargs))
    return wrapper


async def perform_request(session: ClientSession, url: str) -> str:
    async with session.get(url) as response:
        return await response.text()


@run_async
async def fetch_pages(urls: List[str]) -> List[str]:
    async with ClientSession() as session:
        tasks = [perform_request(session, url) for url in urls]
        return await asyncio.gather(*tasks)
