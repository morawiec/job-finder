import asyncio
from typing import List

from mongoengine import connect

from job_finder.config import AppConf as cfg
from job_finder.job import Job
from job_finder.requests import run_async


class AbstractRepository:

    def __init__(self):
        connect(db=cfg.MONGODB_DB,
                name=cfg.MONGODB_USERNAME,
                password=cfg.MONGODB_PASSWORD,
                host=cfg.MONGODB_HOST,
                port=cfg.MONGODB_PORT)


class JobRepository(AbstractRepository):

    @classmethod
    async def add_job(cls, job_dict: dict) -> Job:
        return Job(**job_dict).save()

    @classmethod
    async def get_job_by_job_id(cls, job_id: str) -> List[Job]:
        return Job.objects(job_id=job_id)


async def insert_job_to_database(repo: JobRepository, job: dict):
    exists = await repo.get_job_by_job_id(job.get("job_id"))
    if not exists:
        return await repo.add_job(job)


@run_async
async def insert_jobs_to_database(jobs: List[dict]):
    repo = JobRepository()
    tasks = [insert_job_to_database(repo, job) for job in jobs]
    return await asyncio.gather(*tasks)
